package com.pacifica.calculetaxe.services.impl;

import com.pacifica.calculetaxe.common.enumerations.ProductTypeEnum;
import com.pacifica.calculetaxe.models.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TestUtils {

    public static Invoice getInvoice() {
        Invoice invoice = new Invoice();
        List<InvoiceDetails> invoiceDetails = new ArrayList<>();
        invoiceDetails.add(getChocolateInvoiceDetail());
        invoiceDetails.add(getBookInvoiceDetail());
        invoiceDetails.add(getOtherInvoiceDetail());
        invoice.setInvoiceDetails(invoiceDetails);
        invoice.setTotalProductsTaxe(BigDecimal.valueOf(5.53));
        invoice.setTotaleProductsPrices(BigDecimal.valueOf(48.05));
        return invoice;
    }

    public static InvoiceDetails getChocolateInvoiceDetail() {
        InvoiceDetails InvoiceDetails = new InvoiceDetails();
        InvoiceDetails.setPriceTTC(BigDecimal.valueOf(2.55));
        InvoiceDetails.setProduct(getChocolateProduct());
        InvoiceDetails.setQuantity(3);
        return InvoiceDetails;
    }

    public static InvoiceDetails getBookInvoiceDetail() {
        InvoiceDetails InvoiceDetails = new InvoiceDetails();
        InvoiceDetails.setPriceTTC(BigDecimal.valueOf(27.5));
        InvoiceDetails.setProduct(getBookProduct());
        InvoiceDetails.setQuantity(2);
        return InvoiceDetails;
    }

    public static InvoiceDetails getOtherInvoiceDetail() {
        InvoiceDetails InvoiceDetails = new InvoiceDetails();
        InvoiceDetails.setPriceTTC(BigDecimal.valueOf(18));
        InvoiceDetails.setProduct(getOthersProduct());
        InvoiceDetails.setQuantity(1);
        return InvoiceDetails;
    }

    public static Order getOrder() {
        Order order = new Order();
        List<OrderDetails> OrderDetails = new ArrayList<>();
        OrderDetails.add(getFirstOrderLine());
        OrderDetails.add(getSecondOrderLine());
        OrderDetails.add(getThirdOrderLine());
        order.setOrderDetails(OrderDetails);
        return order;
    }

    public static OrderDetails getFirstOrderLine() {
        OrderDetails ligneCommandes = new OrderDetails();
        ligneCommandes.setProduct(getBookProduct());
        ligneCommandes.setQuantity(2);
        ligneCommandes.setImportLibelle("");
        return ligneCommandes;
    }

    public static OrderDetails getSecondOrderLine() {
        OrderDetails ligneCommandes = new OrderDetails();
        ligneCommandes.setProduct(getOthersProduct());
        ligneCommandes.setQuantity(1);
        ligneCommandes.setImportLibelle("");
        return ligneCommandes;
    }

    public static OrderDetails getThirdOrderLine() {
        OrderDetails ligneCommandes = new OrderDetails();
        ligneCommandes.setProduct(getChocolateProduct());
        ligneCommandes.setQuantity(3);
        ligneCommandes.setImportLibelle("");
        return ligneCommandes;
    }

    public static Product getChocolateProduct() {
        Product product = new Product();
        product.setImported(false);
        product.setLibelle("barres de chocolat");
        product.setUnitPriceWithoutTaxe(BigDecimal.valueOf(0.85));
        product.setProductType(ProductTypeEnum.FOOD);
        return product;
    }

    public static Product getBookProduct() {
        Product product = new Product();
        product.setImported(false);
        product.setLibelle("livres ");
        product.setUnitPriceWithoutTaxe(BigDecimal.valueOf(12.49D));
        product.setProductType(ProductTypeEnum.BOOK);
        return product;
    }

    public static Product getOthersProduct() {
        Product product = new Product();
        product.setImported(false);
        product.setLibelle("CD musical");
        product.setUnitPriceWithoutTaxe(BigDecimal.valueOf(14.99));
        product.setProductType(ProductTypeEnum.OTHERS);
        return product;
    }
}
