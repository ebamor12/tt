package com.pacifica.calculetaxe.service;

import com.pacifica.calculetaxe.models.Invoice;
import com.pacifica.calculetaxe.models.Order;

import java.io.IOException;

public interface InvoiceService {

    Invoice createInvoiceFromOrder(Order order, InvoiceDetailsService invoiceDetailsService, TaxeCalculatorService taxeCalculatorService);

    String createInvoiceFile(Invoice invoice) throws IOException;
}
