package com.pacifica.calculetaxe.common.constant;

public final class FileConstant {

    public static final String FIRST_COMMAND_FILE_PATH                          ="src/test/files/FirstInputCommande.txt";
    public static final String THIRD_COMMAND_FILE_PATH                          ="src/test/files/ThirdInputCommande.txt";
    public static final String SECOND_COMMAND_FILE_PATH                         ="src/test/files/SecondInputCommande.txt";
    public static final String FIRST_EXPECTED_OUTPUT_FILE_PATH                  ="src/test/files/FirstOutputExpected.txt";
    public static final String  TEST_PATH                                       ="src/test/files/";
    public static final String FILE_NAME                                        ="Facture_";
    public static final String TXT_EXTENSION                                    =".txt";
    public static final String DATE_PATTERN                                     ="yyyy-MM-dd HH-mm-ss";
    public static final String CHARSET                                          ="windows-1252";

    private FileConstant(){

    }
}
